package treinamentorestassured;

import dominio.Usuario;
import com.aventstack.extentreports.*;
import com.aventstack.extentreports.reporter.*;
import org.testng.*;
import org.testng.annotations.*;
import java.lang.reflect.*;
import java.util.Random;
import static org.hamcrest.Matchers.*;
import static io.restassured.RestAssured.*;



public class PetStoreTests {

    public static ExtentReports EXTENT_REPORT = null;
    public static ExtentReporter HTML_REPORTER = null;
    public static ExtentTest TEST;
    public static String reportPath = "target/reports/";
    public static String fileName = "TreinamentoRestAssured.html";

    @BeforeSuite
    public void beforeSuite() {
        EXTENT_REPORT = new ExtentReports();
        HTML_REPORTER = new ExtentHtmlReporter(reportPath + "/" + fileName);
        EXTENT_REPORT.attachReporter(HTML_REPORTER);
    }

    @BeforeMethod
    public void beforeMethod(Method method) {
        TEST = EXTENT_REPORT.createTest(method.getName());
    }

    @AfterMethod
    public void afterTest(ITestResult result) {
        switch (result.getStatus()) {
            case ITestResult.FAILURE:
                TEST.log(Status.FAIL, result.getThrowable().toString());
                break;
            case ITestResult.SKIP:
                TEST.log(Status.SKIP, result.getThrowable().toString());
                break;
            default:
                TEST.log(Status.PASS, "Sucesso");
                break;
        }
    }

    @AfterSuite
    public void afterSuite() {
        EXTENT_REPORT.flush();
    }

    @DataProvider(name = "dataUserValidObjectProvider")
    public Object[] dataUserValidProvider() {

        Random random = new Random();

        //Usuario1
        Usuario user1 = new Usuario();
        user1.setId(random.nextInt());
        user1.setUsername("marcelohenrique");
        user1.setFirstName("Marcelo");
        user1.setLastName("Henrique");
        user1.setEmail("marcelohenriquees@gmail.com");
        user1.setPassword("teste123");
        user1.setPhone("37991682156");
        user1.setUserStatus(0);

        //Usuario2
        Usuario user2 = new Usuario();
        user2.setId(random.nextInt());
        user2.setUsername("joao_alves");
        user2.setFirstName("joaO");
        user2.setLastName("Alves");
        user2.setEmail("joao.alves@gmail.com");
        user2.setPassword("Joao5456");
        user2.setPhone("5487451");
        user2.setUserStatus(1);

        //Usuario3
        Usuario user3 = new Usuario();
        user3.setId(random.nextInt());
        user3.setUsername("lauane.lauane");
        user3.setFirstName("lAuAne");
        user3.setLastName("S0ouza");
        user3.setEmail("lauane_d@outlook.com");
        user3.setPassword("lau@S0uz4");
        user3.setPhone("4848481");
        user3.setUserStatus(400);

        //Usuario4
        Usuario user4 = new Usuario();
        user4.setId(random.nextInt());
        user4.setUsername("larissa-oliveira");
        user4.setFirstName("Laris sa");
        user4.setLastName("Oli veira");
        user4.setEmail("lara_1@gmail.com");
        user4.setPassword("1$#%%@22%O");
        user4.setPhone("(37) 9158-7485");
        user4.setUserStatus(43243);

        return new Usuario[]{user1, user2, user3, user4};
    }

    @Test(dataProvider = "dataUserValidObjectProvider")
    public void adicionarUsuarioComSucesso(Usuario user) {

        given().
                baseUri("https://petstore.swagger.io/v2").
                header("content-type", "application/json").
                basePath("/user").
                body(user).
                when().
                post().
                then().
                statusCode(200).
                body("code", equalTo(200),
                        "type", equalTo("unknown"),
                        "message", not(empty()));
    }

    @Test(dataProvider = "dataUserValidObjectProvider")
    public void buscarUsuarioCadastrado (Usuario user) {
        baseURI = "https://petstore.swagger.io/v2";

        given().
                baseUri(baseURI).
                basePath("/user/{username}").
                pathParam("username", user.getUsername()).
                when().
                get().
                then().
                statusCode(200).
                body("username", equalTo(user.getUsername()),
                        "firstName", equalTo(user.getFirstName()),
                        "lastName", equalTo(user.getLastName()),
                        "email", equalTo(user.getEmail()),
                        "password", equalTo(user.getPassword()),
                        "phone", equalTo(user.getPhone()),
                        "userStatus", equalTo(user.getUserStatus()));
    }
}
